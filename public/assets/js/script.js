let canvas = document.querySelector('canvas');
let context = canvas.getContext('2d');
window.ctx = canvas.getContext("2d");

const WIDTH = 300;
const HEIGHT = 200;
const FACING_DOWN = 0;
const FACING_LEFT = 1;
const FACING_RIGHT = 2;
const FACING_UP = 3;
const FRAME_LIMIT = 12;
const playerframes = 6;
const gravityVal = 2.5;

const chipWidth = 32;
const chipHeight = 32;

let currentDirection = FACING_DOWN;
const CYCLE_LOOP = [0, 1, 2, 3 , 4];


let scene = "main";
let currentLoopIndex = 0;
let positionX = 200;
let positionY = 200;

let enemyPos = 1100;
let camerax = -200; //starting camera state
let health = 0;


let storedHealth = localStorage.getItem("health");
if(storedHealth){
  health = storedHealth;
  console.log("Health Retrieved");
}
else{
  health = 300;
  localStorage.setItem("health", health);
  console.log("Health Initiated in memory");
}


//Chip variables
let randomX = Math.abs(Math.floor(Math.random() * 1099) - 50);
let randomY = Math.abs(Math.floor(Math.random() * 499) - 50);
let randomChipX = Math.abs(Math.floor(Math.random() * 7));
let randomChipY = Math.abs(Math.floor(Math.random() * 4));

let playerSpeed = 3;

let score = 0;
let scoreCount = 0;
if (score)
{
    scoreCount = score;
}

let frameCount = 5;
let playerinitial = new Date().getTime();
let playerCurrent = 0;
let playerCurrentFrame = 0;

let gamesEnd = false;


///////////////////////////////////////////////////  Seggsy Images and Shtuff /////////////////////////////////////////////////////////////////////////////
function GameObject(spritesheet, x, y, width, height, virtualX ) {
  this.spritesheet = spritesheet;
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.movementDirection = "None";
  this.virtualX = virtualX;
}


let chipSprite = new Image();
chipSprite.src = "assets/Images/Chip_32x32.png";

let chip = new GameObject(chipSprite, randomX, randomY, 100, 100);

let main = new Image();
main.src = "assets/Images/bg_cool.png";


let robert = new Image();
robert.src = "assets/Images/spritesheet_1.png";

let robertPlayer = new GameObject(robert, 390, 390, 170, 200, 390);


let enemy1 = new Image(); // 128 x 270
enemy1.src = "assets/Images/enemyanimate.png";

let enemy1NPC = new GameObject(enemy1, 1100, 390, 128, 270);



let dieScreen = new Image();
dieScreen.src = "assets/Images/Badoom.png";

let endScreen = new GameObject(dieScreen, 0, 0 , 1200, 500)




// GameObject holds positional information
// Can be used to hold other information based on requirements

//background scrolling
console.log ("scenes loaded");

function animate()
  {
    playerCurrent = new Date().getTime();
    if (playerCurrent - playerinitial >= 200) {
        playerCurrentFrame = (playerCurrentFrame + 1) % frameCount;
        playerinitial = playerCurrent;
    }
    // spritesheet (new image name), x, y, width, height all on spritesheet, backend
    // x, y, width, height, on canvas, front end (what we see)
}
///////////////////////////////////  Movement Shtuff And tings  /////////////////////////////////////////////////////////////////////////////////////////////////

function GamerInput(input) {
  this.action = input; // Hold the current input as a string
}

let gamerInput = new GamerInput("None"); //No Input

function playerBounds()
{
    //only move camera in the side scenes, not top downs
    if (scene == "main")
    {
        //camera follows player
        if (robertPlayer.x > 900 ) 
        {
            camerax -= 5;
        }
        if (robertPlayer.x < 200 ) 
        {
            camerax += 5;
        }

        //doesn't allow player to become out of bounds (moves the screen, not the player)
        if (robertPlayer.x >= 900 ) 
        {
            robertPlayer.x = 900; 
        }
        if (robertPlayer.x <= 200 ) 
        {
            robertPlayer.x = 200;
        }
    }   
    else
    {
        //doesn't allow player to become out of bounds (moves the screen, not the player)
        if (robertPlayer.x >= 920 ) 
        {
            robertPlayer.x = 920;
        }
        if (robertPlayer.x <= 230 ) 
        {
            robertPlayer.x = 230;
        }
        if (robertPlayer.y >= 540 ) 
        {
            robertPlayer.y = 540;
        }
        if (robertPlayer.y <= 10 ) 
        {
            robertPlayer.y = 10;
        }
    }
}

function cameraMovement()
{
    //when the camera reaches the end, stop moving the camera
    if (scene == "main")
    {
        if (camerax >= 200)
        {
            camerax = 200;
        }
        else if (camerax <= -24000)
        {
        camerax = -24000;
        }
    }
}



function update(){

  playerBounds();
  animate();
  let hasMoved = false;
  //console.log(player);
    // Check Input
if (gamerInput.action === "Left") {
      if (robertPlayer.x < 0){
          console.log("player at left edge");
          health -= 1;
          localStorage.setItem("health", health)
      }
      else{
        robertPlayer.x -= playerSpeed; // Move Player Left
        robertPlayer.virtualX -= playerSpeed;
      }
      currentDirection = 2;
  } else if (gamerInput.action === "Right") {
      if (robertPlayer.x > canvas.width){
        console.log("player at right edge");
        health -= 1;
        localStorage.setItem("health", health)
      }
      else{
        robertPlayer.x += playerSpeed; // Move Player Right
        robertPlayer.virtualX += playerSpeed;
      }
      currentDirection = 3;
  } else if (gamerInput.action === "None") {
  }
}

function gravity(){

  if (robertPlayer.y < canvas.height)
  {
      robertPlayer.y += gravityVal;
  }
}

function updateenemyPosition(){
    if(enemyChasingPlayer == true){

      let stunned = new Boolean();
      stunned = false;

      if (robertPlayer.x > enemy1NPC.x)
      {
        console.log("robert is left of NPC");
        if (stunned == false){
          enemy1NPC.x  += 2;
        }
      } 

      if (robertPlayer.x < enemy1NPC.x)
      {
        console.log("robert is right of NPC");
        if (stunned == false){
          health -= 1;
          enemy1NPC.x -= 2;   
        }
      }

      if (robertPlayer.x == enemy1NPC.x){
        console.log("Player & Enemy are at same X pos");
        stunned = true;
        // revert stunned to false based on a timer or based on a distance?
        if(gamerInput.action == "Right"){
          enemy1NPC.x -= 4;
        }
      }

    else{
      console.log("Nothing Happening");
    }
  }

    if (camerax <= -300 && camerax >= -1400) //did about 1400 pixels for the width of the npc sprite, better safe than sorry
    { 
      context.drawImage(enemy1NPC.spritesheet, 1400, 390, enemy1NPC.width, enemy1NPC.height, enemy1NPC.x, enemy1NPC.y, enemy1NPC.width, enemy1NPC.height);
    }
}



function input(event) {
if (event.type === "keydown") {
  switch (event.keyCode) {
      case 37: // Left Arrow
          gamerInput = new GamerInput("Left");
          break; //Left key
      case 38: // Up Arrow
          gamerInput = new GamerInput("Up");
          break; //Up key
      case 39: // Right Arrow
          gamerInput = new GamerInput("Right");
          break; //Right key
      case 40: // Down Arrow
          gamerInput = new GamerInput("Down");
          break; //Down key
      case 87: // w key
          gamerInput = new GamerInput("Up");
          break; //Up key
      case 65: // a
          gamerInput = new GamerInput("Left");
          break; //Left key
      case 83: // s
          gamerInput = new GamerInput("Down");
          break; //Down key
      case 68: // d
          gamerInput = new GamerInput("Right");
          break; //Right key
      default:
          gamerInput = new GamerInput("None"); //No Input
  }
} else {
  gamerInput = new GamerInput("None");
  speed = 2;
}
}



///////////////////////////////////////////  Mobile Controls  //////////////////////////////////////////////////////////////////////////

function pressUp(){
  gamerInput = new GamerInput("Up");
};
function pressLeft(){
  gamerInput = new GamerInput("Left");
};
function pressRight(){
  gamerInput = new GamerInput("Right");
};
function noMoving(){
  gamerInput = new GamerInput("None");
}


/*
var dynamic = nipplejs.create({
  color: 'purple'
});


dynamic.on('added', function (evt, nipple) {
  //nipple.on('start move end dir plain', function (evt) {
  nipple.on('dir:up', function (evt, data) {
     //console.log("direction up");
     gamerInput = new GamerInput("Up");
  });
  nipple.on('dir:down', function (evt, data) {
      //console.log("direction down");
      gamerInput = new GamerInput("Down");
   });
   nipple.on('dir:left', function (evt, data) {
      //console.log("direction left");
      gamerInput = new GamerInput("Left");
   });
   nipple.on('dir:right', function (evt, data) {
      //console.log("direction right");
      gamerInput = new GamerInput("Right");
   });
   nipple.on('end', function (evt, data) {
      //console.log("movement stopped");
      gamerInput = new GamerInput("None");
   });
});
*/
window.addEventListener('keydown', input);

window.addEventListener('keyup', input);

///////////////////////////////////  UI STUFF  //////////////////////////////////////////////////////////////
function drawHealthbar() {
  var width = 100;
  var height = 20;
  var maxHealth = 300;
  
  // Draw the background
  context.fillStyle = "green";
  context.fillRect(0, 0, width, height);
  
  // Draw the fill
  context.fillStyle = "#00FF00";
  var fillVal = Math.min(Math.max(health / maxHealth, 0), 1);
  context.fillRect(0, 0, fillVal * width, height);
}

function writeScore(){
  let scoreString = "score: " + scoreCount;
  context.font = '22px sans-serif';
  context.fillText(scoreString, 1100, 30)
}

function randoPos(rangeX, rangeY, delta){
  this.x = Math.abs(Math.floor(Math.random() * rangeX) - delta);
  this.y = Math.abs(Math.floor(Math.random() * rangeY) - delta);
}



chipPosition = new randoPos(499, 499, 50);


function newChip(){
  randomChipX = Math.abs(Math.floor(Math.random() * 1));
  randomChipXSelect = randomChipX * 64;
  randomChipY = Math.abs(Math.floor(Math.random() * 1));
  randomChipYSelect = randomChipY * 64;
  ChipPosition = new randoPos(499, 10, 50);
}


function manageChip(){
  // place the piece of food
  context.drawImage(chipSprite, randomChipX*64, randomChipY*64, 64, 64, chipPosition.x, chipPosition.y, chipWidth, chipHeight);
  // check for collision (eating)

  if (chipPosition.x < robertPlayer.x && //collision from left to right
  chipPosition.x + chipWidth > robertPlayer.x // collision from right to left
  ){
    console.log("collision!");
        scoreCount ++;
        localStorage.setItem("score", scoreCount);
        newChip();
  }

  console.log("chip placed")
  console.log(chipPosition.x)
  console.log(chipPosition.y)
  // place a new chip
}



let enemyChasingPlayer = new Boolean();

function collisionCheck(){
if ((enemy1NPC.x - 50) < robertPlayer.x + robertPlayer.width && //collision from left to right
  enemy1NPC.x + enemy1NPC.width > robertPlayer.x// collision from right to left
  ){
    console.log("player within enemy perimiter");
    enemyChasingPlayer = true;
  }
  else{
    enemyChasingPlayer = false;
  }
}

function deathScreen(){
  if (health == 0){

    context.clearRect(0, 0, canvas.width, canvas.height);

    context.drawImage(dieScreen.spritesheet, dieScreen.x, dieScreen.y, 1200, 500 ) // Crashes the game lol, feature not a bug

    ctx.drawImage(dieScreen, 10, 10)
  }
}


function draw()
{


  manageChip();
  deathScreen();
  playerBounds();
  updateenemyPosition();
  collisionCheck();
 
  context.drawImage(main, camerax,0,25512,600);
  context.drawImage(robertPlayer.spritesheet, (100 * playerCurrentFrame), 100, 80,85, robertPlayer.x, robertPlayer.y, robertPlayer.width, robertPlayer.height);
  console.log("Robert Position; "+ robertPlayer.x)
  //console.log("Enemy Position ; "+ enemy1NPC.x)
  //console.log("Robert virtual Position; "+ robertPlayer.virtualX)
  context.drawImage(enemy1NPC.spritesheet, 0, 0, enemy1NPC.width, enemy1NPC.height, enemy1NPC.x, enemy1NPC.y, enemy1NPC.width, enemy1NPC.height);
  
}

function gameLoop(){
  cameraMovement();
  update();
  draw();
  writeScore();
  drawHealthbar();
  window.requestAnimationFrame(gameLoop);
}

window.requestAnimationFrame(gameLoop);